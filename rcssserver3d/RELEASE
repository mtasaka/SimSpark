RELEASE News of rcssserver3d-0.7.6

New changes for the RoboCup 2023 competition:

* Rule Changes:
    - A ball holding penalty will be issued if an agent keeps the ball for too
      long ("BallHoldMaxTime" and "BallHoldGoalieMaxTime"). If the ball travels
      more than "BallHoldMaxDistance" meters while being held, a penalty will be
      issued too. The penalty will only be issued if an opponent is closer than
      "BallHoldOppDistance". An agent is considered to be holding the ball if,
      during PlayOn mode, the ball is within "BallHoldRadius" meters of the
      agent and no opponents are closer to the ball. "BallHoldResetTime" defines
      the time after which the timer for ball holding for an agent resets.
    - The Nao robot percepts vision every second cycle.
    - The Nao robot is now equipped with a catch effector on each lower arm.
      When activated the ball sticks to the effector given the ball is near and
      its velocity is low enough. These limits fluctuate. The ball may only be
      caught for the time specified with "CatchTime". A cooldown of
      "CatchCooldownTime" is required to reset the timer.
    - The penalty shootout format got an update. A kicker and a goalie from
      both teams join simultaneously. The agents kick alternately. The ball is
      placed at a fixed position. The agent is beamed to a position behind the
      ball. The agent is only allowed to touch the ball once. The time for a
      single shot is limited. The server determines automatically if the
      penalty shootout as a whole is over. There's no need to restart the server
      between two kicks anymore.

You can get the package on the Simspark page on Gitlab
at https://robocup-sim.gitlab.io/SimSpark/
